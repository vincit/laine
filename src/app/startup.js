define(['jquery', 'knockout', './router', 'marked', 'bootstrap', 'knockout-projections'],
  function($, ko, router, marked) {

  // Components can be packaged as AMD modules, such as the following:
  ko.components.register('nav-bar', { require: 'components/nav-bar/nav-bar' });
  ko.components.register('home-page', { require: 'components/home-page/home' });

  // ... or for template-only components, you can just point to a .html file directly:
  ko.components.register('about-page', {
    template: { require: 'text!components/about-page/about.html' }
  });

  ko.components.register('db', { require: 'components/db/db' });

  ko.components.register('transfer', { require: 'components/transfer/transfer' });

  ko.components.register('single-post', { require: 'components/single-post/single-post' });

  ko.components.register('pagination', { require: 'components/pagination/pagination' });

  ko.components.register('address-service', { require: 'components/address-service/address-service' });

  ko.components.register('tag-page', { require: 'components/tag-page/tag-page' });

  ko.components.register('year-page', { require: 'components/year-page/year-page' });

  ko.components.register('month-page', { require: 'components/month-page/month-page' });

  ko.components.register('post-page', { require: 'components/post-page/post-page' });

  ko.components.register('generic-route', { require: 'components/generic-route/generic-route' });

  ko.components.register('config-service', { require: 'components/config-service/config-service' });

  ko.components.register('template-service', { require: 'components/template-service/template-service' });

  ko.components.register('ga-service', { require: 'components/ga-service/ga-service' });

  // [Scaffolded component registrations will be inserted here. To retain this feature, don't remove this comment.]

  // Set Markdown parser options
  marked.setOptions({
    smartypants: true
  });

  // Start the application
  ko.applyBindings({ route: router.currentRoute });
});
