define(
  [
    "knockout",
    "crossroads",
    "hasher",
    "./routes",
    "../components/config-service/config-service",
    "../components/ga-service/ga-service"
  ], function(ko, crossroads, hasher, routes, configService, gaService) {

  // This module configures crossroads.js, a routing library. If you prefer, you
  // can use any other routing library (or none at all) as Knockout is designed to
  // compose cleanly with external libraries.
  //
  // You *don't* have to follow the pattern established here (each route entry
  // specifies a 'page', which is a Knockout component) - there's nothing built into
  // Knockout that requires or even knows about this technique. It's just one of
  // many possible ways of setting up client-side routes.
  return new Router({
    routes: routes
  });

  function Router(config) {
    var currentRoute = this.currentRoute = ko.observable({});
    this.crossroads = crossroads;
    this.hasher = hasher;

    ko.utils.arrayForEach(config.routes, function(route) {
      var addedRoute = crossroads.addRoute(route.url, function(requestParams) {
        currentRoute(ko.utils.extend(requestParams, route.params));
      });

      if (!_.isUndefined(route.rules)) {
        addedRoute.rules = route.rules;
      }
    });

    activateCrossroads();
  }

  function activateCrossroads() {
    function parseHash(newHash, oldHash) {
      crossroads.parse(newHash);

      // Send google analytics page event if it's enabled
      if (configService.useGa) {
        gaService.sendPageEvent();
      }
    }

    crossroads.normalizeFn = crossroads.NORM_AS_OBJECT;
    hasher.prependHash = '!';
    hasher.initialized.add(parseHash);
    hasher.changed.add(parseHash);
    hasher.init();
  }
});