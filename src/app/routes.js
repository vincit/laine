define([], function() {
  var YEAR_RE = /^\d{4}$/;
  var MONTH_RE = /^\d{2}$/;
  var PAGENUMBER_RE = /^\d*$/;

  return [
    // Home view
    // This is above page view because page view will catch the rest
    {
      url: ':pageNumber:',
      params: { page: 'home-page', mode: 'home' },
      rules: {
        pageNumber: PAGENUMBER_RE
      }
    },

    // Page view
    {
      url: '{slug}',
      params: { page: 'post-page', mode: 'page' }
    },

    // Single post view
    {
      url: '{year}/{month}/{day}/{slug}',
      params: { page: 'post-page', mode: 'post' },
      rules: {
        year: YEAR_RE,
        month: MONTH_RE
      }
    },

    // Year archive
    {
      url: 'archives/{year}/:pageNumber:',
      params: { page: 'year-page', mode: 'year' },
      rules: {
        year: YEAR_RE,
        pageNumber: PAGENUMBER_RE
      }
    },

    // Month archive
    {
      url: 'archives/{year}/{month}/:pageNumber:',
      params: { page: 'month-page', mode: 'month' },
      rules: {
        year: YEAR_RE,
        month: MONTH_RE,
        pageNumber: PAGENUMBER_RE
      }
    },

    // Tag archive
    {
      url: 'tag/{slug}/:pageNumber:',
      params: { page: 'tag-page', mode: 'tag' },
      rules: {
        pageNumber: PAGENUMBER_RE
      }
    }
  ];
});
