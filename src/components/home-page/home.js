define(
  [
    'knockout',
    'text!./home.html',
    '../generic-route/generic-route',
    '../db/db'
  ],
  function(ko, templateMarkup, GR, DB) {

  function HomePage(route) {
    var self = this;

    self.page = ko.observable(1);
    self.posts = DB.posts;

    GR.initPage(route, self.page);
    GR.sync();
  }

  // This runs when the component is torn down. Put here any logic necessary to clean up,
  // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
  HomePage.prototype.dispose = function() {};

  templateMarkup = GR.resolveTemplate(templateMarkup, 'home');

  return { viewModel: HomePage, template: templateMarkup };

});
