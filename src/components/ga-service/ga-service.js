define(
  [
    'knockout',
    '../config-service/config-service'
  ], function(ko, configService) {

  function GaService() {
    var self = this;

    // The GA object that can be used in other components
    self.ga = null;

    if (configService.useGa) {
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', configService.webPropertyID, 'auto');
      self.ga = ga;
    }

    self.sendPageEvent = function() {
      self.ga('send', 'pageview', {
        // Send the hash with the URL to differentiate different page hits
        'page': location.pathname + location.hash
      });
    };
  }

  // This runs when the component is torn down. Put here any logic necessary to clean up,
  // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
  GaService.prototype.dispose = function() { };

  return new GaService();

});
