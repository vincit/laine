define(['knockout'], function(ko) {

  function Transfer(params) {
    this.loadIndex = function() {
      return $.get('index');
    };

    this.loadPost = function(slug) {
      return $.get('posts/' + slug + '.md');
    };

    this.loadPage = function(slug) {
      return $.get('pages/' + slug + '.md')
    };
  }

  // This runs when the component is torn down. Put here any logic necessary to clean up,
  // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
  Transfer.prototype.dispose = function() {};

  return new Transfer();
});
