define(['knockout', 'text!./nav-bar.html', '../db/db', '../address-service/address-service', '../config-service/config-service'],
  function(ko, template, DB, addressService, configService) {

  function NavBarViewModel(params) {
    var self = this;

    self.AS = addressService;
    self.blogName = configService.blogName;
    self.route = params.route;
    self.pages = ko.pureComputed(function() {
      return _.filter(DB.pages(), function(page) {
        return !page().isHidden();
      });
    });
  }

  return { viewModel: NavBarViewModel, template: template };
});
