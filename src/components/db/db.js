define(['knockout', '../transfer/transfer', 'moment'], function(ko, transfer, moment) {

  function DB(params) {
    var self = this;

    // Has the index been loaded yet?
    self.synced = ko.observable(false);

    // List of all posts
    self.posts = ko.observableArray();
    // List of all pages
    self.pages = ko.observableArray();

    // Below are a few pure computeds for convenience, they should be autobuilt
    // when the posts / pages are loaded

    // All posts in a dict (slug as key) for faster retrieval of single post
    self.postDict = ko.pureComputed(function() {
      var ret = {};
      _.forEach(self.posts(), function(post) {
        ret[post().slug()] = post;
      });
      return ret;
    });

    // Dict of pages (slug as key)
    self.pageDict = ko.pureComputed(function() {
      var ret = {};
      _.forEach(self.pages(), function(page) {
        ret[page().slug()] = page;
      });
      return ret;
    });

    // Dict of tags (tag name as key) and the posts they have
    self.tags = ko.pureComputed(function() {
      var ret = {};
      _.forEach(self.posts(), function(post) {
        _.forEach(post().tags(), function(tag) {
          if (!(tag in ret)) {
            ret[tag] = ko.observableArray([post]);
          }
          else {
            ret[tag].push(post);
          }
        });
      });

      return ret;
    });

    // Dict of years (year number as key) and the posts for those years
    self.years = ko.pureComputed(function() {
      var ret = {};
      _.forEach(self.posts(), function(post) {
        var key = post().date().format('YYYY');

        if (!(key in ret)) {
          ret[key] = ko.observableArray([post]);
        }
        else {
          ret[key].push(post);
        }
      });
      return ret;
    });

    // Dict of months ('YYYY-MM' as key) and the posts for those months
    self.months = ko.pureComputed(function() {
      var ret = {};
      _.forEach(self.posts(), function(post) {
        var key = post().date().format('YYYY-MM');

        if (!(key in ret)) {
          ret[key] = ko.observableArray([post]);
        }
        else {
          ret[key].push(post);
        }
      });
      return ret;
    });

    /**
     * Parse a line of tags separated by commas and return the tags in a list.
     */
    self.parseTagLine = function(tagStr) {
      var tags = tagStr.split(',');
      var retTags = [];
      _.forEach(tags, function(tag) {
        retTags.push(tag.trim());
      });
      return retTags;
    };

    /**
     * Parse data of a single post from a regex match, returning an observable
     */
    self.parsePostData = function(matched) {
      var tags = {};

      if (!_.isUndefined(matched[3])) {
        tags = self.parseTagLine(matched[3]);
      }

      var date = moment(matched[1], 'YYYY-MM-DD');
      var slug = matched[2];

      var post = ko.observable({
        slug: ko.observable(slug),
        date: ko.observable(date),
        tags: ko.observable(tags),

        // True if this posts title and content have been fetched from the
        // individual post file
        synced: ko.observable(false),

        // These will be parsed later from the individual post file
        title: ko.observable(null),
        content: ko.observable(null),
        isSplit: ko.observable(null),
        shortContent: ko.observable(null),
        isPage: ko.observable(false)
      });
      return post;
    };

    /**
     * Parse data of a single page from a regex match, returning an observable
     */
    self.parsePageData = function(matched) {
      var slug = matched[1];

      var isHidden = _.isUndefined(matched[2]);
      var linkText = isHidden? '' : matched[2];

      var page = ko.observable({
        slug: ko.observable(slug),
        isHidden: ko.observable(isHidden),
        linkText: ko.observable(linkText),

        synced: ko.observable(false),
        title: ko.observable(null),
        content: ko.observable(null),
        shortContent: ko.observable(null),
        isSplit: ko.observable(false),
        isPage: ko.observable(true)
      });
      return page;
    };

    /**
     * Parse the index file, putting its contents into the given
     * observableArrays
     */
    self.parseIndex = function(posts, pages, indexStr) {
      var post_data_regex = /^(\d{4}-\d{2}-\d{2}) ([a-z0-9\-]+)(\s([^,]+?,?)*)?$/;
      var page_data_regex = /^([a-z0-9\-]+)(?: (.*))?$/;

      var lines = indexStr.split('\n');
      var line_no = 1;

      _.forEach(lines, function(line) {
        var post_match = post_data_regex.exec(line);
        var page_match = page_data_regex.exec(line);

        if (post_match !== null) {
          posts.push(self.parsePostData(post_match));
        }
        else if (page_match !== null) {
          pages.push(self.parsePageData(page_match));
        }
        else {
          console.log('Ignoring unmatched line ' + line_no + ': "' + line + '"');
        }

        ++line_no;
      });

      // After loading all posts, sort them descending by date. Pages will be
      // left as-is
      self.posts.sort(function(left, right) {
        return left().date().isSame(right().date())? 0
            : (left().date().isBefore(right().date())? 1 : -1);
      });
    };

    /**
     * Parse data from a data file, updating the given observable
     */
    self.parseData = function(obj, dataStr) {
      var lines = dataStr.split('\n');
      obj().title(lines[0]);
      obj().synced(true);
      obj().content(lines.slice(2).join('\n'));

      var parts = obj().content().split('<!--SPLIT-->');
      obj().shortContent(parts[0]);
      obj().isSplit(parts.length > 1);
    };

    /**
     * Parse index data and store it in the system.
     */
    self.loadIndex = function() {
      transfer.loadIndex().success(function(data) {
        self.parseIndex(self.posts, self.pages, data);
        self.synced(true);
      });
    };

    /**
     * Load a post with the given slug and update its data in the index.
     */
    self.loadPost = function(slug) {
      transfer.loadPost(slug).success(function(data) {
        var post = self.postDict()[slug];
        self.parseData(post, data);
      });
    };

    /**
     * Load a page with the given slug and update its dat a in the index.
     */
    self.loadPage = function(slug) {
      transfer.loadPage(slug).success(function(data) {
        var page = self.pageDict()[slug];
        self.parseData(page, data);
      });
    }
  }

  // This runs when the component is torn down. Put here any logic necessary to clean up,
  // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
  DB.prototype.dispose = function() {};

  return new DB();
});
