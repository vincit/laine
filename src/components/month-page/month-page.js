define(['knockout', 'text!./month-page.html', '../generic-route/generic-route', '../db/db'],
  function(ko, templateMarkup, GR, DB) {

  function MonthPage(route) {
    var self = this;

    self.month = route.year + '-' + route.month;
    self.page = ko.observable(1);
    self.posts = ko.pureComputed(function() {
      var months = DB.months();

      if (self.month in months) {
        return months[self.month]();
      }
      else {
        return [];
      }
    });

    GR.initPage(route, self.page);
    GR.sync();
  }

  // This runs when the component is torn down. Put here any logic necessary to clean up,
  // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
  MonthPage.prototype.dispose = function() {};

  return { viewModel: MonthPage, template: templateMarkup };

});
