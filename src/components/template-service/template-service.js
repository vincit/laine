define(['knockout'], function(ko) {

  function TemplateService() {
    var self = this;

    self.templateStore = {};

    self.loadTemplate = function(templateName, templateUrl) {
      if (templateName in self.templateStore) {
        return self.templateStore[templateName];
      }
      else {
        var html = $.ajax(templateUrl, {
          // We need to set async false to receive the template before rendering
          // starts
          async: false,
          dataType: 'html'
        }).responseText;

        self.templateStore[templateName] = html;
        return html;
      }
    };
  }

  // This runs when the component is torn down. Put here any logic necessary to clean up,
  // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
  TemplateService.prototype.dispose = function() { };

  return new TemplateService();

});
